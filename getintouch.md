---
layout: page
title: "Get in touch"
permalink: /contact/
---
You can get in touch with me through the following places:

## Social media

**Twitter**: [Weavelss](https://twitter.com/weavelss)

**LinkedIn**: [Rik van Tuijl](https://www.linkedin.com/in/rik-van-tuijl-17464b13/) (no recruiters please, I'm fine where I am)

## E-mail:

[rik@weavel.nl](mailto:rik@weavel.nl)

If you want to use PGP to sign and encrypt your message, my PGP key is [0xB9AC4274445A86D9 on pgp.surf.nl](https://pgp.surf.nl/pks/lookup?op=get&search=0xB9AC4274445A86D9).

## Code

Also check out my public code repository on [Gitlab](https://gitlab.com/weavel)! I also use [Github](https://github.com/weavel) but only when I really have to (looking at you, Ansible Galaxy).

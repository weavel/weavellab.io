---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: page
---
Welcome to my little corner on the internet. This website showcases who I am, what I do and what interests me. This site may also contains random unsorted blog posts about my work, projects and scripts.

## Who am I?

My name is Rik van Tuijl, system administrator for a Dutch health/education institution. I'm passionate about IT, in particular networking and system administration on an enterprise scale. I'm also interested in site reliability engineering, automation, CI/CD, identity and access management and related policy.

## What makes me tick?

In my spare time I'm interested and involved in a lot of things:

- I organize a [LAN-party](https://www.zuydlan.nl)!
- I'm passionate about home automation, using Home Assistant
- I love doing DYI-projects. This ranges from electronics and Arduino to craft beer.

Expect these subjects to be featured on this website.

# Get in touch

If you want to get in touch with me, please review the contact page about where to best find me on the internet!
